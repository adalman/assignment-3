<!DOCTYPE html>
<html>
   <head>
      <title>Registrar's Office Class Details</title>
   </head>
   <body>
      <h1>Registrar's Office</h1>
   <hr>
      <h2>Class Details (class id {{details.getClassId()}})</h2>
      <strong>Course Id: </strong>{{details.getCourseId()}}<br>
      <strong>Days: </strong>{{details.getDays()}}<br>
      <strong>Start time: </strong>{{details.getStarttime()}}<br>
      <strong>End time: </strong>{{details.getEndtime()}}<br>
      <strong>Building: </strong>{{details.getBldg()}}<br>
      <strong>Room: </strong>{{details.getRoomnum()}}<br>
   </hr>
   <hr>
      <h2>Course Details (course id {{details.getCourseId()}})</h2>
      % itemlist = deptstring.split(" ")
      % for i in range(0, len(itemlist) - 1, 2):
         <strong>Dept and Number: </strong>{{itemlist[i]}} {{itemlist[i + 1]}}<br>
      % end
      <strong>Area: </strong>{{details.getArea()}}<br>
      <strong>Title: </strong>{{details.getTitle()}}<br>
      <strong>Description: </strong>{{details.getDescription()}}<br>
      <strong>Prerequisites: </strong>{{details.getPrereqs()}}<br>
      <strong>Professor(s): </strong>{{details.getProfnames()}}<br>
   </hr>
   <hr>
   
   <p>
   Click here to do <a href = searchresults>another class search</a>
   </p>
   </hr>
   % include ('footer.tpl')
   <hr></hr>
   </body>
</html>