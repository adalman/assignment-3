#!/usr/bin/env python

#-----------------------------------------------------------------------
# database.py
# Author: Bob Dondero
#-----------------------------------------------------------------------

from sqlite3 import connect
from sys import stderr
from os import path
from row import Row
from details import Details

#-----------------------------------------------------------------------

class Database:
    
    def __init__(self):
        self._connection = None

    def connect(self):      
        DATABASE_NAME = 'reg.sqlite'

        if not path.isfile(DATABASE_NAME):
            errorMsg = "Database connection failed"
            self.print_error(errorMsg)
            return errorMsg
        self._connection = connect(DATABASE_NAME)
        return None
                    
    def disconnect(self):
        self._connection.close()

    def print_error(self, message):
        print >>stderr, "database: " + str(message)

    def remove_wildcards(self, keyword):

        if keyword is None:
            return 

        if '%' in keyword:
            return keyword.replace('%', '[%]')
        elif '_' in keyword:
            return keyword.replace('_', '[_]')
        else:
            return keyword

    def search(self, keywords):
        
        cursor = self._connection.cursor()
        errorMsg = None

        ## THIS IS JUST THE ORIGINAL CLASS LIST
        strmStr = ''

        dept = ''
        coursenum = ''
        area = ''
        title = ''

        for x in keywords.keys():
            #print(x)
            if x == '-dept':
                dept = keywords[x]
            elif x == '-coursenum':
                coursenum = keywords[x]
            elif x == '-area':
                area = keywords[x]
            elif x == '-title':
                title = keywords[x]

        args = []
        #print(dept + " " + coursenum + ' ' + area + ' ' + title)
        stmStr = '''
        SELECT classid, dept, coursenum, area, title
        FROM classes, courses, crosslistings
        WHERE classes.courseid = courses.courseid
        AND crosslistings.courseid = classes.courseid
        '''

        if dept != '' and dept is not None:
            stmStr = stmStr + '''AND crosslistings.dept LIKE ? '''
            dept = self.remove_wildcards(dept)
            temp = '%' + dept.lower() + '%'
            args.append(temp)

        if coursenum != '' and coursenum is not None:
            stmStr = stmStr + 'AND crosslistings.coursenum LIKE ?' + '\n'
            coursenum = self.remove_wildcards(coursenum)
            temp = '%' + coursenum.lower() + '%'
            args.append(temp)

        if area != '' and area is not None:
            stmStr = stmStr + 'AND courses.area LIKE ?' + '\n'
            area = self.remove_wildcards(area)
            temp = '%' + area.lower() + '%'
            args.append(temp)

        if title != '' and title is not None:
            stmStr = stmStr + 'AND courses.title LIKE ?' + '\n'
            title = self.remove_wildcards(title)
            temp = '%' + title.lower() + '%'
            args.append(temp)

        stmStr += 'ORDER BY crosslistings.dept ASC, crosslistings.coursenum ASC, classes.classid ASC'
        
        try:
            cursor.execute(stmStr, args)
            rows = cursor.fetchall()
        except Exception, e:
            errorMsg = e
            self.print_error(e)

        results = []
        for row in rows:
            temp = Row(str(row[0]), row[1], row[2], row[3], row[4])
            results.append(temp)

        cursor.close()
        return results, errorMsg

    def searchDetails(self, classid):

        cursor = self._connection.cursor()
        errorMsg = None

        stmStr = '''
        SELECT classes.courseid, days, starttime, endtime, bldg, roomnum
        FROM classes, courses
        WHERE classes.courseid = courses.courseid
        AND classes.classid = ?
        '''
        try:
            cursor.execute(stmStr, [classid])
            rows = cursor.fetchone()
        except Exception, e:
            errorMsg = e
            self.print_error(e)
            return None, errorMsg

        if rows is None:
            return None, errorMsg
            
        details = Details(classid)
        details.setClassDetails(rows)
 
        stmStr = '''
        SELECT dept, coursenum
        FROM classes, courses, crosslistings
        WHERE classes.courseid = courses.courseid
        AND courses.courseid = crosslistings.courseid
        AND classes.classid = ?
        ORDER BY crosslistings.dept ASC, crosslistings.coursenum ASC
        '''
        try:
            cursor.execute(stmStr, [classid])
            rows = cursor.fetchall()
        except Exception, e:
            errorMsg = e
            self.print_error(e)
            return None, errorMsg

        for i in range(0, len(rows)):
            row = rows[i]
            details.addDept(row[0], row[1])

        stmStr = '''
        SELECT area, title, descrip, prereqs
        FROM classes, courses
        WHERE classes.courseid = courses.courseid
        AND classes.classid = ?
        '''
        try:
            cursor.execute(stmStr, [classid])
            row = cursor.fetchone()
        except Exception, e:
            errorMsg = e
            self.print_error(e)
            return None, errorMsg

        details.setCourseDetails(row)

        stmStr = '''
        SELECT profname
        FROM classes, courses, coursesprofs, profs
        WHERE classes.courseid = courses.courseid
        AND courses.courseid = coursesprofs.courseid
        AND coursesprofs.profid = profs.profid
        AND classes.classid = ?
        ORDER BY profs.profname ASC
        '''
        try: 
            cursor.execute(stmStr, [classid])
            rows = cursor.fetchall()
        except Exception, e:
            errorMsg = e
            self.print_error(e)
            return None, errorMsg

        for row in rows:
            for x in row:
                details.addProf(str(x))

        return details, errorMsg

    

#-----------------------------------------------------------------------

# For testing:

if __name__ == '__main__':
    database = Database()
    database.connect()
    rows = database.search('')
    for row in rows:
        print row
    database.disconnect()