#!/usr/bin/env python

#-----------------------------------------------------------------------
# dept.py
# Author: Ami Berman and Ally Dalman
#-----------------------------------------------------------------------

class Dept:

    def __init__(self, dept, num):
        self._dept = str(dept)
        self._num = str(num)

    def __str__(self):
        return self._dept + " " + self._num


