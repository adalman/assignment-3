#!/usr/bin/env python

#-----------------------------------------------------------------------
# details.py
# Author: Ami Berman and Ally Dalman
#-----------------------------------------------------------------------

class Details:

    def __init__(self, classid):
        self._classid = classid
        self._deptlist = ''
        self._profstring = ''

    def getClassId(self):
        return self._classid

    def setClassDetails(self, classdetails):
        self._courseid = classdetails[0]
        self._days = classdetails[1]
        self._starttime = classdetails[2]
        self._endtime = classdetails[3]
        self._bldg = classdetails[4]
        self._roomnum = classdetails[5]

    def getCourseId(self):
        return self._courseid

    def getDays(self):
        return self._days

    def getStarttime(self):
        return self._starttime

    def getEndtime(self):
        return self._endtime

    def getBldg(self):
        return self._bldg

    def getRoomnum(self):
        return self._roomnum

    def addDept(self, dept, num):
        self._deptlist += (str(dept) + " " + str(num) + " ")

    def getDepts(self):
        return self._deptlist

    def setCourseDetails(self, coursedetails):
        self._area = coursedetails[0]
        self._title = coursedetails[1]
        self._descrip = coursedetails[2]
        self._prereqs = coursedetails[3]

    def getArea(self):
        return self._area

    def getTitle(self):
        return self._title

    def getDescription(self):
        return self._descrip

    def getPrereqs(self):
        return self._prereqs

    def addProf(self, profname):
        if self._profstring != '':
            self._profstring += ' , '

        self._profstring += profname

    def getProfnames(self):
        return self._profstring




