#-----------------------------------------------------------------------
# runserver.py
# Authors: Ally Dalman and Ami Berman
#-----------------------------------------------------------------------

from sys import argv
from database import Database
from time import localtime, asctime, strftime
from urllib import quote_plus
from bottle import route, request, response, error, redirect, run
from bottle import template, TEMPLATE_PATH

TEMPLATE_PATH.insert(0, '')

@route('/')
@route('/index')
def index():
    
    initialKeywords = {}
    database = Database()
    database.connect()
    rows, errorMsg = database.search(initialKeywords)
    templateInfo = {
        'errorMsg': errorMsg,
        'rows': rows}
    return template('searchform.tpl', templateInfo)

def cookieHandler(cookie, keywords):
    if cookie == "detailspage":

        dept = request.get_cookie('dept')
        if dept is None:
            keywords['-dept'] = ''
        else:
            keywords['-dept'] = dept

        coursenum = request.get_cookie('coursenum')
        if coursenum is None:
            keywords['-coursenum'] = ''
        else:
            keywords['-coursenum'] = coursenum

        area = request.get_cookie('area')
        if area is None:
            keywords['-area'] = ''
        else:
            keywords['-area'] = area

        title = request.get_cookie('title')
        if title is None:
            keywords['-title'] = ''
        else:
            keywords['-title'] = title

        print keywords

    response.set_cookie('pageid', 'mainpage')
    response.set_cookie('dept', str(keywords['-dept']))
    response.set_cookie('coursenum', str(keywords['-coursenum']))        
    response.set_cookie('area', str(keywords['-area']))
    response.set_cookie('title', str(keywords['-title']))

    return keywords


@route('/')
@route('/searchresults')
def searchResults():

    keywords = {}

    dept = request.query.get('dept')
    if dept is None:
        keywords['-dept'] = ''
    else:
        keywords['-dept'] = dept

    coursenum = request.query.get('coursenum')
    if coursenum is None:
        keywords['-coursenum'] = ''
    else:
        keywords['-coursenum'] = coursenum

    area = request.query.get('area')
    if area is None:
        keywords['-area'] = ''
    else:
        keywords['-area'] = area

    title = request.query.get('title')
    if title is None:
        keywords['-title'] = ''
    else:
        keywords['-title'] = title

    cookie = request.get_cookie('pageid')
    print cookie
    keywords = cookieHandler(cookie, keywords)

    database = Database()
    errorMsg = database.connect()

    if errorMsg is not None:
        templateInfo = {
        'error': errorMsg
        }
        return template('errormsg.tpl', templateInfo)

    rows, errorMsg = database.search(keywords)
    database.disconnect()
    
    if errorMsg is not None:
        templateInfo = {
        'error': errorMsg
        }
        return template('errormsg.tpl', templateInfo)

    templateInfo = {
        'rows': rows,
        'dept': keywords['-dept'],
        'coursenum': keywords['-coursenum'],
        'area': keywords['-area'],
        'title': keywords['-title']}
    return template('searchform.tpl', templateInfo)

@route('/coursedetails')
def loadCourseDetails():

    keywords = {}

    classid = request.query.get('classid')
    if classid is None or classid == '':
        templateInfo = {
        'error': "Missing class id"
        }
        return template('errormsg.tpl', templateInfo)

    if not classid.isdigit():
        templateInfo = {
        'error': "Classid is not numeric"
        }
        return template('errormsg.tpl', templateInfo)

    response.set_cookie('pageid', 'detailspage')
    dept = request.get_cookie('dept')
    if dept is None:
        dept = ''
    response.set_cookie('dept', str(dept))

    coursenum = request.get_cookie('coursenum')
    if coursenum is None:
        coursenum = ''
    response.set_cookie('coursenum', str(coursenum))

    area = request.get_cookie('area')
    if area is None:
        area = ''
    response.set_cookie('area', str(area))

    title = request.get_cookie('title')
    if title is None:
        title = ''
    response.set_cookie('title', str(title))

    database = Database()
    errorMsg = database.connect()

    if errorMsg is not None:
        templateInfo = {
        'error': errorMsg
        }
        return template('errormsg.tpl', templateInfo)

    details, errorMsg = database.searchDetails(classid)
    database.disconnect()

    if errorMsg is not None:
        templateInfo = {
        'error': errorMsg
        }
        return template('errormsg.tpl', templateInfo)

    if details is None:
        templateInfo = {
        'error': "Class id " + str(classid) + " does not exist."
        }
        return template('errormsg.tpl', templateInfo)
    
    templateInfo = {
        'details': details,
        'deptstring': details.getDepts()}
    return template('coursedetails.tpl', templateInfo)

@error(404)
def notFound(error):
    return template('notfound.tpl')

if __name__ == '__main__':
    if len(argv) != 2:
        print 'Usage: ' + argv[0] + ' port'
        exit(1)
    run(host='0.0.0.0', port=argv[1], debug=True)