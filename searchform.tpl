<!DOCTYPE html>
<html>
   <head>
      <title>Registrar's Office Class Search</title>
   </head>
   <body>
      <h1>Registrar's Office</h1>
      <h2>Class Search</h2>
      <hr>
      <form action="searchresults" method="get">
         <table>
            <tr>
               <td>Dept:</td> 
               <td><input type="text" name="dept" value = {{dept}}></td>
            </tr>
            <tr>
               <td>Course Num:</td> 
               <td><input type="text" name="coursenum" value = {{coursenum}}></td>
            </tr>
            <tr>
               <td>Area:</td> 
               <td><input type="text" name="area" value = {{area}}></td>
            </tr>
            <tr>
               <td>Title:</td>
               <td><input type="text" name="title" value = {{title}}></td>
            </tr>
            <tr>
               <td></td>
               <td><input type="submit" value="Submit"></td>
            </tr>
         </table>
      </form>
      <hr>
         <table>
               <tr>
                  <td><strong/>ClassId</strong></td>
                  <td><strong/>Dept</strong></td>
                  <td><strong/>Num</strong></td>
                  <td><strong/>Area</strong></td>
                  <td><strong/>Title</strong></td>
               </tr>
               % for row in rows:
               <tr>
                  <td align = "left"><a href=coursedetails?classid={{row.getClassId()}}>{{row.getClassId()}}</a></td>
                  <td align = "left">{{row.getDept()}}</td>
                  <td align = "left">{{row.getNum()}}</td>
                  <td align = "left">{{row.getArea()}}</td>
                  <td align = "left">{{row.getTitle()}}</td>
               </tr>
               % end
         </table>
      </hr>
      % include('footer.tpl')
   </hr>
   <hr></hr>
   </body>
</html>