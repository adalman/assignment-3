<!DOCTYPE html>
<html>
   <head>
      <title>Registrar's Office Class Search</title>
   </head>
   <body>
      <h1>Registrar's Office</h1>
      <h2>Class Search</h2>
      <hr>
      <form action="searchresults" method="get">
         <table>
            <tr>
               <td>Dept:</td> 
                  <td><input type="text" name="dept"></td>
            </tr>
            <tr>
               <td>Course Num:</td> 
               <td><input type="text" name="coursenum"></td>
            </tr>
            <tr>
               <td>Area:</td> 
               <td><input type="text" name="area"></td>
            </tr>
            <tr>
               <td>Title:</td>
               <td><input type="text" name="title"></td>
            </tr>
            <tr>
               <td></td>
               <td><input type="submit" value="Submit"></td>
            </tr>
         </table>
      </form>
      <hr>
         <table>
               <tr>
                  <th>ClassId </th>
                  <th>Dept  </th>
                  <th>Num  </th>
                  <th>Area  </th>
                  <th align = "left">Title  </th>
               </tr>
               % for row in rows:
               <tr>
                  <td align = "left"><a href=''> {{row.getClassId()}}</a></td>
                  <td align = "left">{{row.getNum()}}</td>
                  <td align = "left">{{row.getDept()}}</td>
                  <td align = "left">{{row.getArea()}}</td>
                  <td align = "left">{{row.getTitle()}}</td>
               </tr>
               % end
         </table>
      </hr>
      <br>
      % include('footer.tpl')
   </hr>
   </body>
</html>